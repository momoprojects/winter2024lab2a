import java.util.Random;
public class Calculator{
	public static int add(int num1, int num2){
		return num1+num2;
	}
	public static double sroot(int num){
		return Math.sqrt(num);
	}
	public static int randomNum(){
		Random random = new Random();
		int randomNum = random.nextInt();
		return randomNum;
	}
	public static double divide( int dividend, int divisor){
		if (dividend==0|| divisor==0) return 0;
		else{
        return ((double)dividend)/((double)divisor) ;
		}
	}
}