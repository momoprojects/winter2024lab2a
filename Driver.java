import java.util.Scanner;
public class Driver{
	public static void main(String[] args){
		
		Scanner sc= new Scanner(System.in);
		
		System.out.println("hello user!");
		System.out.println("Please input two whole numbers to add together.");
		
		System.out.println("First number:");
		int chosenNumber1= sc.nextInt();
		sc.nextLine();
		
		System.out.println("Second number:");
		int chosenNumber2= sc.nextInt();
		sc.nextLine();
		
		Calculator Calculator= new Calculator();
		
		System.out.println("Result is:"+Calculator.add(chosenNumber1,chosenNumber2));
		
		System.out.println("Please input one number to find out its square root:");
		chosenNumber1= sc.nextInt();
		sc.nextLine();
		
		System.out.println("Result: " + Calculator.sroot(chosenNumber1));
		
		System.out.println("Please input one number to divide by the second number:");
		
		System.out.println("First number:");
		chosenNumber1= sc.nextInt();
		sc.nextLine();
		
		System.out.println("Second number:");
		chosenNumber2= sc.nextInt();
		sc.nextLine();
		
		System.out.println("Result: "+Calculator.divide(chosenNumber1,chosenNumber2));
		
		
		System.out.println("Here is a random number: "+Calculator.randomNum());
	}
}